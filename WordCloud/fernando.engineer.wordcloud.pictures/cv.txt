Fernando Jose Capeletto Neto
Electronics Systems Engineer, Software Development Engineer  
Achievements - Cover Letter - GPS Curriculum

              
Brazilian-Italian Citizenship
i.am@fernando.engineer


EXPERIENCES


Software Development Engineer @ TEAMS @ TBA Group (Konecranes)
Nov/2020 - present - Düsseldorf, Germany

TEAMS is the TBA's Equipment Control System (ECS) fleet management for automated equipment for container handling at terminal ports.
Activities: Collaborate in a dynamic team using an Agile Scrum (SAFe) methodology producing testable and efficient code, in a continuous delivery environment for the development of distributed, scalable, and highly available applications for TEAMS while keeping myself updated as an engineer with software development edge technologies.
Environment: ActiveMQ, Avro, C++, Confluence, Corba, Eclipse/IntelliJ, Git & GitLab, Hibernate, Java 8, Jenkins, JIRA, JSON, Kafka, Linux, Liquibase, Maven, Microservices, Mockito, Netbeans, Oracle, Python, REST API, Spring, SQL Server




Electronics Systems Engineer Officer (Captain) @ Brazilian Navy
Mar/2010 - Aug/2020 - (~70% Brazil ~30% France)

Ten years of experience working with Softwares, Hardware, and Systems for Naval and Defense domains.
More than seven years on the Brazilian Navy Submarines Development Program - PROSUB (including three years overseas), working on its specification, development, tests, predictive, corrective, and evolutive maintenance tasks, and its IVVQ cycle as well as supplying technical subsidies for management decision making and strategic planning.




Engineering Advisor at General-Coordination of Sensitive Goods at the Ministry of Science, Technology, Innovations of Brazil - Nuclear Area Specialist - (Brazilian Navy representative)
Oct/2019 - Jul/2020 - Brasilia, DF, Brazil

Technical Analysis of equipment, materials, software, and technologies to promote the non-proliferation of nuclear weapons through the implementation of Nuclear Suppliers Group (NSG) Guidelines for the trade of nuclear and nuclear-related goods.



Electronics Systems Engineer Advisor for the Brazilian Navy Submarines Development Program - Combat System Specialist @ Brazilian Navy
May/2015 - Sep/2019 -Rio de Janeiro, RJ, Brazil

Supervise at the shipyard the processes of Development, Construction, Assembly, Inspections, Quality Assurance (QA), Commissioning, Tests & Trials for the Brazilian Submarines Class Scorpéne (SBR) in the Brazilian Navy Submarines Development Program (PROSUB) scope.
Analysis, Preparation, Execution, and Reports of the Assembly, Installation, Set-to-Work, Tests and Trials Specifications and Protocols (at Factory, Harbor, and Sea), for Equipment, Softwares, Systems, and Systems of Systems (SoS) of the SBRs.
Creator and System Administrator of SISCAPELA (Management Software to Audit and Control the industrial lifecycle for the submarine construction (see projects).
Combat Systems (CS) - System of Systems Engineering work: HW & SW Technical, Interface and Requirements Specs., Detailed Design Review for Architecture,  Integration, Functionalities & Performance related to the CS and its SubSystems (i.e., Navigation, Combat System Management, External and Internal Communications, Underwater Acoustic,  Air-Surface Detection (e.g. Attack & Optronic Periscopes , Radar and Communication Electronic Support Measures) and Weapons Control). (C4ISR topics)
Full Stack DevOps of SW for evolutionary maintenance for the SBR CS (see AIS-MK0 project below).




Software Development Engineer of Combat Management System for Brazilian Submarines Development Program @ Brazilian Navy
Aug/2012 - Mar/2015 - PROSUB Office in France (ET-PROSUB) - Toulon, VAR, France

Engineer of the Brazilian Navy in charge of receive Transfer of Knowledge (ToK) and Transfer of Technology (ToT) (a partnership between France & Brazil) of the Combat System SUBTICS®. (a Combat Management SoS with millions of code lines, distributed architecture, and heavy constraints on performances and reliability, based on GALAXI® JAVA EE Framework). (Activities @ Naval Group - former DCNS/THALES Group)
Full Stack DevOps of SW for evolutionary maintenance for the SBR CS (see MOAS CASE project below).
System and Software Development and Documentation using V-Model SDLC in compliance with MIL-STD-498 standard:
System Engineering (Requirements Analysis, Functional Analysis/Allocation Specification, and Traceability, System Tests Procedures, Tests Campaign, and Reports);
System Design (Architecture, Components, and System Interfaces);
Software Engineering (Software Requirements Specification, Software Tests Procedures, Test Campaign Design and Running Reports at Unit, Integration, System and Acceptance levels);
Software Architecture Design (Software Components, Software Modeling and Software Interfaces);
Software Development and Frameworks Analysis, Detecting, Reproducing, Debugging and Solving of functional problems related to the system architecture.
Environment: IBM Rational: Dynamic Object Oriented Requirements System (DOORS), Software Modeler (RSM), Clearcase; HP Quality Center (QC), Eclipse IDE, Java Core and Frameworks (e.g GEMO (DCNS Framework), Java RMI, Spring), CORBA, DDS-DLRL pattern.



Electronics Systems Engineer Advisor @ Engineering Department @ Superintendence of Systems @ Navy Weapon Systems Directorship @ Brazilian Navy
Jan/2011 - Jul/2012 - Rio de Janeiro, RJ, Brazil

Writing and Analysis of Technical Notes, Reports, and Specification Documents for the acquisition, modernization, replacement, and installation of Electronic and Weapon Systems equipment for Military Organizations and naval assets of the Brazilian Navy;
Writing, Running, Analysis, and Reports about the Set-to-Work, Harbor Acceptance Trials, and Sea Acceptance Trials of equipment and systems.
Integrated Logistic Support: Analysis regarding the built-in configuration, allocation, maintenance sheets, and replacement interchangeability feasibility of parts, their component items, and suppliers.
Modernization for the Class Tupi Submarines Combat System (See MODSUB Project below).
PRODUCTS

SISCAPELA:

Idealizer, Architect, Full Stack DevOps, and SysAdmin of a Management Information System for the SBR Submarines’ industrial life cycles, its component items, and functional circuits.
Integrates and processes data from Shipyard’s departments.
Show the technical drawings according to the build situation.
Verify the requirements’ completeness for launching.
Ecosystem: Front-End: Web Application (JavaScript, PHP & HTML5) and JavaSE; Back-End: Linux, Java, PostgreSQL/PLSQL, Python & VBA. (2018/2019)
AIS-MK0:

Full Stack Development of Tactical and Kinematics Situations Displayer for Vessels.
Decodes VHF NMEA0183 messages received through a PC serial RS-232.
Records and Broadcasts contacts detected by an AIS Receiver to the Combat System Network
Ecosystem: JavaEE, GALAXI®, Framework, Spring & Eclipse IDE. (2015/2017)
MOAS CASE:

System & Software Architect, Full Stack Developer, Integrator and IVVQ Tester of Mines and Obstacles Avoidance Sonar System
Requirement specification, modeling, coding, testing, integration, verification and validation activities
Provided evolutive maintenance for the SUBTICS® Combat System adding a new sensor from scratch using the V-model development Process.
Ecosystem: DOORS, Eclipse IDE, IBM Rational (Software Architect & Clearcase), HP Quality Center, JavaEE, GALAXI®, Spring, Wireshark Dissector (2013/2015)
Combat System Simulators and Stimulators:

Development of Equipment and Interfaces simulators belonging to the Combat Systems Network (i.e., Simulator of Electronic Warfare Measurement Equipment).
Ecosystem: Eclipse IDE, JavaSE , Wireshark (2012/2013)
MODSUB (Combat System Modernization of Class Tupi Submarines):

Replacement of the Ferranti KAFS A10 Shooting and Tactical Data Detection System and the CSU83 Sonar by Lockheed Martin 'AN/BYG 501 MOD 1D' Combat System) (2011)
Elaboration of Technical Specification for the Installation of the Attack Simulator at CIAMA (Submarinists Instruction Center) (Based on Autodesk AutoCAD®)
Analysis of the User and Equipment Technical Manuals and of the Protocols for Sea Acceptance Trials for the Modernized Combat System.
Running the Sea Acceptance Trials for the new Combat System installed in Submarine Tapajó (S-33) (10 days on the sea / 86 hours of immersion).
Writing Technical Requirements Specification and Interface Agreement between the 'AN/BYG-501 Mod 1D' Combat Management System and 'AR-900(V)1' Electronic Support Measures, aiming to the Combat Management System to Electronic Support Measures integration, onboard on Brazilian Navy Tupi and Tikuna Classes Submarines.
CercaSys:

Integration of a legacy Navy ́s alarms system for fences monitoring of restricted areas, into a PC:
Developed prototyped a Signal Integration Card for PC via DB25 parallel interface.
Ecosystem: C++, Cadence OrCAD PCB Designer and OrCAD Pspice Designer (2010)
Connectmed.com:

Web Development of portals with content and tools aimed at the health sector (e.g. association of doctors in Brazil, São Paulo association of medicine)
Web Development of portal aimed to the B2B between hospitals and health insurers.
Chosen by the magazine "Exame/Negocios" (Sep/2000) as 'one of the five most promising ideas in the Brazilian Internet for the medical field.
Ecosystem: Perl, PHP & Oracle. (2000)
EDUCATION AND CERTIFICATES

sep/2013 - mar/2015 CMS Software Development Team OJT - Phase 2 - in France - Naval Group

sep/2012 - jul/2013 CMS Deputy Technical Manager OJT - Phase 1 - in France - Naval Group 

2009 B.Sc. in Electronic Systems Engineering, Escola Politécnica - Universidade de São Paulo
   - Certified Translation (Deutsch)

TECHNICAL SKILLS

(10+ years): Agile, Scrum, C++, Collaborative Work, Distributed Systems, Electronics, Software Engineering, Groupwares, HTML, JavaSE, JavaEE, Labview, Linux, MySQL, OOP, Perl, PHP, PL/SQL, PostgreSQL, Shell Scripting, Software and Web Development, UML, XML; (8+ years): Defense, Leadership, Navigation & Combat Systems; (7+ years): ANT, Full Stack; IBM DOORS and RSA/RSM, HP Quality Center; Frameworks (ActiveMQ, GALAXI, GEMO, Hibernate, Spring, MVC, etc), Requirements Management, MATLAB/Simulink, MIL-STD-498, Systems Engineering, IVVQ, Integration, Versioning (i.e ClearCase, Git, SVN);(3 years):  Data Science, Docker, Jenkins, JupyterLab, JIRA & Confluence, Maven, Microservices, Python (IPython, matplotlib, Numpy, Pandas, Scikit-learn, Seaborn, etc), Spring Boot(1 year): Angular, Kubernetes, Node.js, Visual Studio Code(Self-Learning):Apache Spark 2, ASPICE & ISO26262, AUTOSAR, AWS, Gradle, Groovy, Machine Learning, Microsoft Power BI, Model-Driven Software Design, Model-Based Systems Engineering, Polyspace, R, Real-Time Operating Systems, Scala, Target Language Compiler, Testing in MIL/SIL/PIL and HIL, Timing Architects, VectorPREEvision and vTestStudio.

LANGUAGES

- English: Advanced
- French: Upper-Intermediate
- Portuguese - Native.

